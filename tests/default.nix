let
  pkgs = import ../nix { };

  mkTest = file: pkgs.nixosTest (import file);
in
{
  firewall = mkTest ./firewall.nix;
  resolver = mkTest ./resolver.nix;
}
