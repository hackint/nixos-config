{
  name = "unbound";
  nodes.recursive = {
    imports = [ ../modules/dns-resolver.nix ];
    networking = {
      hostName = "recursive";
      domain = "test";
    };
  };

  nodes.upstream = {
    imports = [ ../modules/dns-resolver.nix ];
    networking = {
      hostName = "upstream";
      domain = "test";
    };

    hackint.resolver.upstreamResolvers = [
      "1.1.1.1"
      "8.8.8.8"
    ];
  };

  testScript = ''
    def test(machine):
        machine.wait_for_unit("unbound.service")

        output = machine.succeed("ss -lpn | grep unbound")
        assert "127.0.1.53:53" in output
        assert "[::1]:53" in output

        output = machine.succeed("cat /etc/resolv.conf")
        assert "nameserver ::1" in output

        machine.shutdown()

    test(recursive)
    test(upstream)
  '';
}
