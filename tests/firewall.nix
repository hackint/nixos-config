{
  name = "firewall";
  nodes.machine = {
    imports = [ ../modules/firewall.nix ];
    hackint.firewall = {
      allowedPorts = {
        tcp = [ 6697 9999 ];
        udp = [ 53 ];
        sctp = [ 6697 ];
      };
      allowedManagementNetworks = {
        ipv4 = [
          "192.168.0.0/24"
        ];
        ipv6 = [
          "2001:db8::/64"
        ];
      };
    };
  };

  testScript = ''
    machine.wait_for_unit("nftables.service")

    machine.log(machine.execute("nft list ruleset")[1])
  '';
}
