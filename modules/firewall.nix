{ lib
, config
, pkgs
, ...
}:

let
  cfg = config.hackint.firewall;

  checkedRuleset = text:
    let
      file = pkgs.writeText "netfilter" text;
      check = pkgs.vmTools.runInLinuxVM (
        pkgs.runCommand "nft-check"
          {
            buildInputs = [ pkgs.nftables ];
            inherit file;
          } ''
          # make sure protocols & services are known
          ln -s ${pkgs.iana-etc}/etc/protocols /etc/protocols
          ln -s ${pkgs.iana-etc}/etc/services /etc/services
          # test the configuration
          nft --file $file
        ''
      );
    in
    "# checked with ${check}\n" + text;
in

{
  options.hackint.firewall = with lib; {
    allowedPorts = {
      tcp = mkOption {
        type = types.listOf types.port;
        default = [ ];
        example = [ 80 443 ];
        description = ''
          TCP listener ports that are not immediately conntracked, but
          first passed through synproxy before going into conntrack.

          SSH is handled implicitly.
        '';
        apply = concatMapStringsSep ", " toString;
      };

      udp = mkOption {
        type = types.listOf types.port;
        default = [ ];
        example = [ 53 123 ];
        description = ''
          UDP listener ports, which are not conntracked.
        '';
        apply = concatMapStringsSep ", " toString;
      };

      sctp = mkOption {
        type = types.listOf types.port;
        default = [ ];
        example = [ 6697 ];
        description = ''
          SCTP listener ports, which are fully conntracked.
        '';
        apply = concatMapStringsSep ", " toString;
      };
    };
    allowedManagementNetworks = {
      ipv4 = mkOption {
        type = types.listOf types.str;
        default = [];
        example = [
          "192.0.2.0/24"
        ];
        description = ''
          List of IPv4 prefixes that are allowed to connect to SSH.
        '';
      };

      ipv6 = mkOption {
        type = types.listOf types.str;
        default = [];
        example = [
          "2001:DB8::/64"
        ];
        description = ''
          List of IPv6 prefixes that are allowed to connect to SSH.
        '';
      };
    };
  };

  config = {
    boot.kernel.sysctl = {
      # limit conntrack resources to ~16 MB
      "net.netfilter.nf_conntrack_max" = 65536;

      # don't infer conntrack sessions from TCP ACKs
      "net.netfilter.nf_conntrack_tcp_loose" = 0;
    };

    networking.firewall.enable = false;
    networking.nftables = {
      enable = true;
      ruleset = checkedRuleset ''
        table inet raw {
          chain prerouting {
            type filter hook prerouting priority raw;
            policy accept;

            ${lib.optionalString (cfg.allowedPorts.tcp != "") ''
            # set notrack for tcp packets for listening sockets
            tcp dport { ${cfg.allowedPorts.tcp} } tcp flags syn notrack
            ''}

            ${lib.optionalString (cfg.allowedPorts.udp != "") ''
            # set notrack for vpn flows
            udp dport { ${cfg.allowedPorts.udp} } notrack
            ''}
          }
        }

        table inet filter {
          ${lib.optionalString (cfg.allowedManagementNetworks.ipv6 != []) ''
          set mgmt6 {
            typeof ip6 saddr;
            flags interval;
            elements = {
              ${lib.concatStringsSep "\n      " cfg.allowedManagementNetworks.ipv6}
            }
          }
          ''}

          ${lib.optionalString (cfg.allowedManagementNetworks.ipv4 != []) ''
          set mgmt4 {
            typeof ip saddr;
            flags interval;
            elements = {
              ${lib.concatStringsSep "\n      " cfg.allowedManagementNetworks.ipv4}
            }
          }
          ''}

          chain input {
            type filter hook input priority filter;
            policy drop;

            iif lo accept;

            ip6 nexthdr ipv6-icmp icmpv6 type { nd-router-advert, nd-neighbor-advert, nd-neighbor-solicit } accept

            jump infrastructure

            ct state established,related accept

            jump synproxies

            ct state invalid drop

            ip6 nexthdr ipv6-icmp icmpv6 type echo-request limit rate 5/second;
            ip protocol icmp icmp type echo-request limit rate 5/second;

            jump applications
          }

          chain infrastructure {
            # stateless

            ${lib.optionalString (cfg.allowedPorts.udp != "") ''
            udp dport { ${cfg.allowedPorts.udp} } accept
            ''}
            ${if (cfg.allowedManagementNetworks.ipv6 != []) then ''
            ip6 saddr @mgmt6 tcp dport { ssh } accept
            '' else ''
            ip6 nexthdr tcp tcp dport { ssh } accept
            ''}
            ${if (cfg.allowedManagementNetworks.ipv4 != []) then ''
            ip saddr @mgmt4 tcp dport { ssh } accept
            '' else ''
            ip protocol tcp tcp dport { ssh } accept
            ''}
          }

          chain synproxies {
            # synproxy incoming tcp connections and only allow them into
            # conntrack, when they finish the 3-way handshake.
            # http://ffmancera.net/nftables/2019/08/04/mitigate-tcp-syn-flood.html
            # https://wiki.nftables.org/wiki-nftables/index.php/Synproxy

            ${lib.optionalString (cfg.allowedPorts.tcp != "") ''
            ip6 nexthdr tcp tcp dport { ${cfg.allowedPorts.tcp} } ct state { invalid, untracked } synproxy mss 1460 wscale 7 timestamp sack-perm
            ip protocol tcp tcp dport { ${cfg.allowedPorts.tcp} } ct state { invalid, untracked } synproxy mss 1440 wscale 7 timestamp sack-perm
            ''}
          }

          chain applications {
            # stateful

            ${lib.optionalString (cfg.allowedPorts.sctp != "") ''
            sctp dport { ${cfg.allowedPorts.sctp} } accept
            ''}
          }

        }
      '';
  };
};
}
