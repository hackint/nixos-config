{ config, lib, pkgs, ... }:

with lib;

{
  options = {
    hackint.acme = {
      nameserver = mkOption {
        type = types.str;
        description = ''
          Nameserver to push RFC2136 updates to.
        '';
      };

      domains = mkOption {
        type = types.listOf types.str;
        description = ''
          Domain names to request certificates for.
        '';
        default = [ config.networking.fqdn ];
      };
    };
  };

  config = {
    # Activation to generate the TSIG Key and Secret on host
    systemd.services."acme-tsig-keygen" = {
      after = [ "systemd-tmpfiles-setup.service" ];
      before = [ "network.target" ];
      partOf = [ "network-pre.target" ];
      wantedBy = [ "multi-user.target" ];

      script = ''
        set -ex
        if ! [ -f /var/lib/acme/update.tsig ]; then
          mkdir -pv /var/lib/acme
          umask 0377
          ${pkgs.knot-dns}/bin/keymgr -t '${config.networking.fqdn}' hmac-sha512 | ${pkgs.yq}/bin/yq -r '.key[0].secret' > /var/lib/acme/update.tsig
        fi
      '';

      serviceConfig = {
        Type = "oneshot";

        User = "acme";
        Group = config.security.acme.defaults.group;

        RemainAfterExit = true;
      };
    };

    security.acme = {
      acceptTerms = true;

      defaults = {
        email = "mail@${config.networking.domain}";

        dnsProvider = "rfc2136";
        credentialsFile = pkgs.writeText "acme-credentials" ''
          LEGO_EXPERIMENTAL_CNAME_SUPPORT=true
          RFC2136_NAMESERVER=${config.hackint.acme.nameserver}
          RFC2136_TSIG_ALGORITHM=hmac-sha512.
          RFC2136_TSIG_KEY=${config.networking.fqdn}
          RFC2136_TSIG_SECRET_FILE=/var/lib/acme/update.tsig
          RFC2136_PROPAGATION_TIMEOUT=3600
        '';
      };

      certs = {
        "${config.networking.fqdn}" = {
          extraDomainNames = config.hackint.acme.domains;
        };
      };
    };
  };
}

