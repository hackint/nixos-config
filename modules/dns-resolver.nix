{ config, lib, ... }:
let
  cfg = config.hackint.resolver;
in
{
  options.hackint.resolver = {
    enable = lib.mkOption {
      default = true;
      type = lib.types.bool;
    };

    upstreamResolvers = lib.mkOption {
      type = lib.types.listOf lib.types.str;
      default = [ ];
    };
  };

  config = lib.mkIf cfg.enable {
    services.resolved.enable = false;
    networking.nameservers = [ "::1" ];
    networking.resolvconf = {
      useLocalResolver = false;
      extraConfig = "nameserver='127.0.1.53'";
    };
    services.unbound = {
      enable = true;
      settings = {
        server.interface = [ "127.0.1.53" "::1" ];
        server.access-control = map (a: "\"${a}\" allow") [
          "::1/128"
          "127.0.0.0/8"
        ];
        forward-zone = lib.mkIf (cfg.upstreamResolvers != [ ]) [
          {
            name = ".";
            forward-addr = cfg.upstreamResolvers;
          }
        ];
      };
    };
  };
}
