{ lib
, config
, ...
}:

with lib;

let
  cfg = config.hackint.boot.loader;
in
{
  options.hackint.boot.loader = {
    efi = {
      enable = mkEnableOption "booting in EFI mode.";

      mountPoint = mkOption {
        type = types.path;
        default = "/boot/efi";
        description = ''
          Path to where your EFI partition is mounted at.
        '';
      };

      useEfiVars = mkOption {
        type = types.bool;
        default = true;
        description = ''
          Whether to use efivars or install to removable media.
        '';
      };
    };

    mbr = {
      enable = mkEnableOption "booting in MBR mode.";

      device = mkOption {
        type = types.str;
        example = "/dev/disk/by-id/scsi-0QEMU_QEMU_HARDDISK_drive-scsi0-0-0-0";
        description = ''
          Disk device to install the boot loader onto.
        '';
      };
    };
  };

  config = {
    assertions = [
      {
        assertion = (cfg.efi.enable -> !cfg.mbr.enable) && (cfg.mbr.enable -> !cfg.efi.enable);
        message = "Cannot enable both `hackint.boot.loader.efi.enable` and `hackint.boot.loader.mbr.enable` support at the same time.";
      }
      {
        assertion = cfg.efi.enable || cfg.mbr.enable;
        message = "Need to enable either `hackint.boot.loader.efi.enable` or `hackint.boot.loader.mbr.enable` support for this machine to boot.";
      }
    ];

    boot.loader.grub = {
      enable = true;
      version = 2;
      configurationLimit = 3;
    } // lib.optionalAttrs (cfg.efi.enable) {
      device = "nodev";
      efiSupport = true;
    } // lib.optionalAttrs (cfg.mbr.enable) {
      device = cfg.mbr.device;
    };

    boot.loader.efi = lib.optionalAttrs (cfg.efi.enable) {
      canTouchEfiVariables = cfg.efi.useEfiVars;
      efiSysMountPoint = cfg.efi.mountPoint;
    };

  };

}
