{
  networking = {
    domain = "staging.hackint.org";
  };

  deployment.tags = [ "staging" ];

  hackint.acme.nameserver = "1.2.3.4";
}
